import React, { Component } from 'react';
import crypto from 'crypto';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      voiceActive: false,
      mic: null,
      stream: null
    }
    this.isOn = false;
    this.chunks = [];
    this.randomKey = crypto.randomBytes(15).toString('hex');

  }

  componentDidMount() {
    navigator.mediaDevices.getUserMedia({audio: true}).then(this.onPermissionGranted);
  }

  onPermissionGranted = (stream) => {
    //const context = new AudioContext();
    //const source = context.createMediaStreamSource(stream);
    const microphone = new MediaRecorder(stream);
    microphone.ondataavailable = this.handleHoldComplete;
    this.setState({
      stream: stream,
      mic: microphone
    })
  }

  handleHoldComplete = (e) => {
    // push new data into chunks
    this.chunks.push(e.data);

    let blob = new Blob(this.chunks, { 'type' : 'audio/ogg; codecs=opus' });
    let formData = new FormData();
    let randomMessageKey = crypto.randomBytes(15).toString('hex');
    formData.append('command', blob, 'command.wav');

    // send query to the API in order to upload our audio file
    fetch('http://0.0.0.0:8888/api/upload/' + this.randomKey + '/' + randomMessageKey, {
      method: 'post',
      body: formData
    }).then(resp => {
      console.log("Uploaded");
      console.log(resp);
    }).catch(err => {
      console.error(err);
    });

    // reset out chunks
    this.chunks = [];
    this.setState({
      voiceActive: false
    })
  }

  handleHoldBegin = () => {
    console.log(this.chunks);
    this.state.mic.start();
    this.setState({voiceActive: true});
  }

  tempToggle = () => {
    this.isOn = !this.isOn;
    if (this.isOn) {
      this.handleHoldBegin();
    } else {
      this.state.mic.stop();
    }
  }

  render() {
    let active = this.state.voiceActive;
    //  onTouchStart={this.handleHoldBegin} onTouchEnd={this.handleHoldComplete} onMouseDown={this.handleHoldBegin} onMouseUp={this.handleHoldComplete}
    return (
      <div id="wrapper">
        <div id="container">
          <h1 id="title">Discover the news.</h1>

          <div id='touch-to-speak' className={active ? "push-down" : undefined} onClick={this.tempToggle}>

          </div>

          <div id="hint">
            {active &&
              <p>Listening...</p>
            }
            {!active &&
              <p>Touch to speak</p>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default App;
