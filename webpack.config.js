var path = require('path');
var webpack = require('webpack');

 module.exports = {
     entry: './src/index.js',
     output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'index.bundle.js'
     },
     module: {
         loaders: [
             {
                 exclude: /node_modules/,
                 test: /\.js$/,
                 loader: 'babel-loader',
                 query: {
                     // stage-2 enables the spread operator
                     presets: ['es2015', 'react', 'stage-2']
                 }
             },
             {
               exclude: /node_modules/,
               test: /\.(pdf|jpg|png|gif|svg|ico)$/,
               loader: 'url-loader'
             }
         ]
     },
     stats: {
         colors: true
     },
     devtool: 'inline-source-map'
 };
